angular.module('starter.controllers', ['firebase'])

.controller('LuggageCtrl', function($scope,$location,$firebaseObject, $firebaseArray,$rootScope,Threads,Auth,$state,Users) {
    $scope.threads=Threads;
    $scope.detail='';

    $scope.addThread=function(weight){
      console.log('adding Luggage thread')
      var user = $firebaseObject(Users.child(Auth.$getAuth().uid));

      user.$loaded().then(function(){
        Threads.$add({
        id:Threads.length,
        creatorID:Auth.$getAuth().uid,
        creatorName:user.nickname,
        tripDesc:"I ROCK",
        tripTitle:"Going to my moms home",
        type:"Luggage",
        weight:weight
      }).then(function(ref){
        console.log('added Thread with id:'+ref.key());
        $state.go('luggage_request');
      })

      });
    }

    $scope.detailShow=function(id){
      $state.go('luggage_request_specific',{threadID:id});
    }

})

.controller('LuggageDCtrl', function($scope,$location,$firebaseObject, $firebaseArray,$rootScope,Threads,Auth,$state,Users) {
    $scope.threads=Threads;
    $scope.threadID=$state.params.threadID;
    var thread = $firebaseObject(Threads.$ref().child($scope.threadID));
    thread.$loaded().then(function(){
      $scope.detailThread=thread;
    });
    $scope.removeThread=function(abc){
      thread.$remove().then(function(){
        console.log('deleted');
        $state.go('luggage_request');
      });
    };
})

.controller('TravelDCtrl', function($scope,$location,$firebaseObject, $firebaseArray,$rootScope,Threads,Auth,$state,Users) {
    $scope.threads=Threads;
    $scope.threadID=$state.params.threadID;
    var thread = $firebaseObject(Threads.$ref().child($scope.threadID));
    var commentThread= $firebaseArray(Threads.$ref().child($scope.threadID).child('comments'));

    thread.$loaded().then(function(){
      $scope.detailThread=thread;
      $scope.comments = thread.comments;
    });

    $scope.addComment=function(theComment){
      console.log('adding comment')
      var user = $firebaseObject(Users.child(Auth.$getAuth().uid));

      user.$loaded().then(function(){
        commentThread.$add({
          name:user.nickname,
          desc:theComment
        })
      });
    }    

})

.controller('TravelCtrl', function($scope,$location,$firebaseObject, $firebaseArray,$rootScope,Threads,Auth,$state,Users) {
    $scope.threads=Threads;
    $scope.addThread=function(travel){
      console.log('adding travel thread')
      var user = $firebaseObject(Users.child(Auth.$getAuth().uid));

      user.$loaded().then(function(){
        Threads.$add({
        id:Threads.length,
        creatorID:Auth.$getAuth().uid,
        creatorName:user.nickname,
        tripDesc:travel.desc,
        tripTitle:travel.title,
        type:"Travel",
        weight:0
      }).then(function(ref){
        console.log('added Thread with id:'+ref.key());
        $state.go('travelfrom');
      })

      });
    }

    $scope.detailShow=function(id){
      console.log(id);
      $state.go('travelDetails',{threadID:id});
    }

})

.controller('LoginCtrl', function($rootScope, $scope,Auth,$location,Users) {
  $scope.user={};


  $scope.createUser = function(user) {
      $scope.message = null;
      $scope.error = null;
      console.log('signingup')

      Auth.$createUser({
        email: user.email,
        password: user.password
      }).then(function(userData) {
        $scope.message = "User created with uid: " + userData.uid;
        console.log($scope.message);
        Users.child(userData.uid).set({
          email:user.email,
          nickname:user.username
        })
        $scope.login(user);
      }).catch(function(error) {
        $scope.error = error;
        console.log($scope.error);
      });
    };  

    $scope.login = function(user) {
      $scope.authData = null;
      $scope.error = null;

      Auth.$authWithPassword({
        email: user.email,
        password: user.password
      }).then(function(authData) {
        $scope.authData = authData;
        $scope.message ="Logged in as:" + authData.uid;
        console.log("Logged in as:", authData.uid);

        Users.child(authData.uid).once("value", function(snap) {
          $rootScope.loggedInUser=snap.val();
          console.log($rootScope.loggedInUser);
        });
        $location.path('/tab/flights')
      }).catch(function(error) {
        $scope.error=error;
        console.error("Authentication failed:", error);
      });
    };    

    $scope.removeUser = function(user) {
      $scope.message = null;
      $scope.error = null;

      Auth.$removeUser({
        email: user.email,
        password: user.password
      }).then(function() {
        $scope.message = "User removed";
      }).catch(function(error) {
        $scope.error = error;
      });
    };

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('ThreadsCtrl', function($scope, $stateParams, Threads, Users) {
  $scope.threads=Threads;
  console.log($scope.threads);
})


.controller('AccountCtrl', function($scope,$state,Auth,Users,$rootScope,Threads) {
    $scope.updateUser= function(user) {
      $scope.message = null;
      $scope.error = null;
      console.log('updatingProfile');
      Users.child(Auth.$getAuth().uid).update({
        'nickname':user.nickname,
        'profileDesc':user.profileDesc,
        'age':user.age
    });

    };

  $scope.addThread=function(){
    console.log('adding thread')
    Threads.$add({
      id:Threads.length,
      creatorID:Auth.$getAuth().uid,
      creatorName:'dickshit',
      tripDesc:"I ROCK",
      tripTitle:"Going to my moms home",
      type:"Travel"
    }).then(function(ref){
      console.log('added Thread with id:'+ref.key());
    })
  }



  $scope.logout=function(){
    console.log($rootScope.loggedInUser);
    $rootScope.loggedInUser={};
    Auth.$unauth();
    $state.go('login');
  }
});
